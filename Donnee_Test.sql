insert into sexe values(DEFAULT,'HOMME');
insert into sexe values(DEFAULT,'FEMME');

insert into acteur values( DEFAULT,'Tom Holland',1,NULL);
insert into acteur values( DEFAULT,'Scarlette Johnson',2,NULL);
insert into acteur values( DEFAULT,'Benedict Cumberbatch ',1,NULL);
insert into acteur values( DEFAULT,'Scarlet Johnson',2,NULL);
insert into acteur values( DEFAULT,'Chadwick Boseman',1,NULL);
insert into acteur values( DEFAULT,'Chris Evan',1,NULL);

insert into emotion values(DEFAULT,'JOIE');
insert into emotion values(DEFAULT,'TRISTESSE');
insert into emotion values(DEFAULT,'PEUR');
insert into emotion values(DEFAULT,'COLERE');
insert into emotion values(DEFAULT,'SURPRISE');
insert into emotion values(DEFAULT,'DEGOUT');
insert into emotion values(DEFAULT,'EMERVEILLEMENT');
insert into emotion values(DEFAULT,'TENSION');
insert into emotion values(DEFAULT,'ANXIETE');
insert into emotion values(DEFAULT,'DECEPTION');
insert into emotion values(DEFAULT,'FRUSTRATION');
insert into emotion values(DEFAULT,'NOSTALGIE');
insert into emotion values(DEFAULT,'COMPASSION');
insert into emotion values(DEFAULT,'EMPATHIE');
insert into emotion values(DEFAULT,'ENTHOUSIASME');
insert into emotion values(DEFAULT,'AMOUR');
insert into emotion values(DEFAULT,'HAINE');
insert into emotion values(DEFAULT,'DESESPOIR');
insert into emotion values(DEFAULT,'SOULAGEMENT');

insert into geste values (DEFAULT,'marcher');
insert into geste values (DEFAULT,'courir');
insert into geste values (DEFAULT,'se tenir droit');
insert into geste values (DEFAULT,'se pencher');
insert into geste values (DEFAULT,'se lever');
insert into geste values (DEFAULT,'se coucher');
insert into geste values (DEFAULT,'se mettre à genoux');
insert into geste values (DEFAULT,'se prosterner');
insert into geste values (DEFAULT,'sourire');
insert into geste values (DEFAULT,'froncer le sourcils');
insert into geste values (DEFAULT,'cligner des yeux');
insert into geste values (DEFAULT,'lever un sourcil');
insert into geste values (DEFAULT,'plisser les yeux');
insert into geste values (DEFAULT,'hausser les épaules');
insert into geste values (DEFAULT,'tirer la langue');
insert into geste values (DEFAULT,'faire un signe de la main');
insert into geste values (DEFAULT,'pointer du doigt');
insert into geste values (DEFAULT,'faire un geste de la tête');
insert into geste values (DEFAULT,'faire un signe de la croix');
insert into geste values (DEFAULT,'faire un geste de salut');
insert into geste values (DEFAULT,'tenir une arme');
insert into geste values (DEFAULT,'prendre une tasse');
insert into geste values (DEFAULT,'ouvrir une porte');
insert into geste values (DEFAULT,'utiliser un téléphone');
insert into geste values (DEFAULT,'écrire sur une feuille de papier');
insert into geste values (DEFAULT,'pleurer');
insert into geste values (DEFAULT,'rire');
insert into geste values (DEFAULT,'frissoner');
insert into geste values (DEFAULT,'trembler');
insert into geste values (DEFAULT,'crier');
insert into geste values (DEFAULT,'bâiller');
insert into geste values (DEFAULT,'faire un poing');
insert into geste values (DEFAULT,'fumer une cigarette');
insert into geste values (DEFAULT,'ajuster une cravate');
insert into geste values (DEFAULT,'frapper');
insert into geste values (DEFAULT,'donner un coup de pied');
insert into geste values (DEFAULT,'lancer un coup de poing');
insert into geste values (DEFAULT,'parer un coup');

insert into projet values ( DEFAULT,'HULK','hulk.jpg','Le docteur Robert Bruce Banner, alias Hulk est un super-heros evoluant dans l univers Marvel de la maison d edition Marvel Comics.en mai 1962.');
insert into projet values ( DEFAULT,'SPIERMAN','spiderman.jpg','Tom Holland, alias Spiderman est un super-heros evoluant dans l univers Marvel de la maison d edition Marvel Comics. ');
insert into projet values ( DEFAULT,'DOCTOR STRANGE','doctorstrange.jpeg','Le docteur Robert Bruce Banner, alias Hulk est un super-heros evoluant dans l univers Marvel de la maison d edition Marvel Comics. .');
insert into projet values ( DEFAULT,'BLACK WIDOW','blackwidow.jpg','Le docteur Robert Bruce Banner, alias Hulk est un super-heros evoluant dans l univers Marvel de la maison d edition Marvel Comics.');
insert into projet values ( DEFAULT,'BLACK PANTHER','blackpanther.jpg','Le docteur Robert Bruce Banner, alias Hulk est un super-heros evoluant dans l univers Marvel de la maison d edition Marvel Comics.');
insert into projet values ( DEFAULT,'CAPITAN AMERICAN','capitalamerican.jpg','Le docteur Robert Bruce Banner, alias Hulk est un super-heros evoluant dans l univers Marvel de la maison d edition Marvel Comics. ');

insert into categorieplateau values (DEFAULT,'Ville');
insert into categorieplateau values (DEFAULT,'Nature');

insert into plateau values (DEFAULT,'Kananaskis, Alberta, Canada','Plateau montagneux situé dans les Rocheuses canadiennes','kananaskis.jpg',1,2);
insert into plateau values (DEFAULT,'Glen Canyon, Arizona, États-Unis','Le Plateau de Glen Canyon est représenté comme une zone désertique et aride de l Arizona','glen.jpg',1,2);
insert into plateau values (DEFAULT,'Monument Valley, Arizona, États-Unis','Le plateau est célèbre pour ses formations rocheuses rouges et brunes','monument.jpg',1,2);
insert into plateau values (DEFAULT,'San Francisco, Californie, États-Unis','Ville très urbaine et très peuplée','sanfransisco.jpg',1,1);
insert into plateau values (DEFAULT,'Gamma Base, Désert du Nevada, États-Unis','Présenté comme un complexe scientifique secret, entouré d un haut mur de sécurité','gamma.jpg',1,2);
insert into plateau values (DEFAULT,'Fort Bragg, Californie, Etats-Unis','Fort Bragg est montree comme la ville ou se trouve la base militaire de Bruce Banner','fort.jpg',1,1);
insert into plateau values (DEFAULT,'Université de Californie à Berkeley','Montrée comme le lieu de travail de deux personnages importants','universite.jpg',1,1);
insert into plateau values (DEFAULT,'Palouse Falls, Washington, Etats-Unis','Le Palouse Falls est une cascade qui se trouve dans une gorge profonde creusee par la riviere Palouse','palouse.jpg',1,2);

insert into plateau values (DEFAULT,'Palouse Falls, Washington, Etats-Unis','Le Palouse Falls est une cascade qui se trouve dans une gorge profonde creusee par la riviere Palouse','palouse.jpg',2,2);
insert into plateau values (DEFAULT,'Fort Bragg, Californie, Etats-Unis','Fort Bragg est montree comme la ville ou se trouve la base militaire de Bruce Banner','fort.jpg',2,1);

insert into scene values (DEFAULT,'Scène 1',5,1);
insert into scene values (DEFAULT,'Scène 2',10,1);
insert into scene values (DEFAULT,'Scène 3',15,1);

insert into action values (DEFAULT,'Roger demande à Ninah de le suivre en lui tenant la main',4,16,35,4);
insert into action values (DEFAULT,'Une personne frappe à la porte',4,8,29,1);
insert into action values (DEFAULT,'Roger discute avec la personne derrière la porte',4,19,30,4);
