/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author P14A_77_Michael
 */
@Entity
public class Planning {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer idscene;
    String nomscene;
    Integer duree;
    Integer idplateau;
    String nom;
    String description;
    String nomimage;
    Integer idprojet;
    Integer idcategorieplateau;
    

    public Integer getIdscene() {
        return idscene;
    }

    public void setIdscene(Integer idscene) {
        this.idscene = idscene;
    }

    public String getNomscene() {
        return nomscene;
    }

    public void setNomscene(String nomscene) {
        this.nomscene = nomscene;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public Integer getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(Integer idplateau) {
        this.idplateau = idplateau;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNomimage() {
        return nomimage;
    }

    public void setNomimage(String nomimage) {
        this.nomimage = nomimage;
    }

    public Integer getIdprojet() {
        return idprojet;
    }

    public void setIdprojet(Integer idprojet) {
        this.idprojet = idprojet;
    }

    public Integer getIdcategorieplateau() {
        return idcategorieplateau;
    }

    public void setIdcategorieplateau(Integer idcategorieplateau) {
        this.idcategorieplateau = idcategorieplateau;
    }
    
    
    
    
}
