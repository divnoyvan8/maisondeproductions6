/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author RickyBic
 */
public class Details_action {

    private Integer idActeur;
    private String phrase;
    private Integer idEmotion;
    private Integer idGeste;

    public Integer getIdActeur() {
        return idActeur;
    }

    public void setIdActeur(Integer idActeur) {
        this.idActeur = idActeur;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public Integer getIdEmotion() {
        return idEmotion;
    }

    public void setIdEmotion(Integer idEmotion) {
        this.idEmotion = idEmotion;
    }

    public Integer getIdGeste() {
        return idGeste;
    }

    public void setIdGeste(Integer idGeste) {
        this.idGeste = idGeste;
    }

}
