/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDao;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import static javax.ws.rs.HttpMethod.GET;
import javax.ws.rs.Path;
import model.Acteur;
import model.CategoriePlateau;
import model.Plateau;
import model.Projet;
import model.Scene;
import model.Action;
import model.Emotion;
import model.Geste;
import model.Planning;
import model.V_planning;
import static org.hibernate.CacheMode.GET;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author
 */
@Controller
public class SpringController {

    @Autowired
    HibernateDao dao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        List<Projet> lp = dao.findAll(Projet.class);
        model.addAttribute("lp", lp);
        return "acceuil";
    }

    @RequestMapping(value = "/planning", method = RequestMethod.GET)
    public String planning(Model model) {
        List<Projet> lp = dao.findAll(Projet.class);
        model.addAttribute("lp", lp);
        return "planning";
    }

    @RequestMapping(value = "/recherche", method = RequestMethod.GET)
    public String recherche(Model model, HttpServletRequest request) {
        Projet s = new Projet();
        String recherche = request.getParameter("recherche");
        List<Projet> sujet = new ArrayList<>();
        sujet = dao.findWhereOr(recherche);
        model.addAttribute("liste", sujet);
        return "listerecherche";
    }

    @RequestMapping(value = "/ajoutscene", method = RequestMethod.GET)
    public String ajoutscene(Model model, @RequestParam(required = true) Integer idScene) {
        model.addAttribute("idPlateau", idScene);
        return "ajoutscene";
    }

    @RequestMapping(value = "/redirectionUpdate", method = RequestMethod.GET)
    public String redirectionUpdate(Model model, HttpServletRequest request) {
        int idActeur = Integer.parseInt(request.getParameter("idAction"));
        int idScene = Integer.parseInt(request.getParameter("idScene"));
        String scenario = request.getParameter("scenario");
        model.addAttribute("idAction", idActeur);
        model.addAttribute("scenario", scenario);
        model.addAttribute("idScene", idScene);
        List<Emotion> emotion = dao.findAll(Emotion.class);
        model.addAttribute("le", emotion);
        List<Geste> geste = dao.findAll(Geste.class);
        model.addAttribute("lg", geste);
        List<Acteur> acteur = dao.findAll(Acteur.class);
        model.addAttribute("lact", acteur);
        return "updateaction";
    }

    @RequestMapping(value = "/listeplateau", method = RequestMethod.GET)
    public String liste_plateau(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        List<CategoriePlateau> lcp = dao.findAll(CategoriePlateau.class);
        model.addAttribute("lcp", lcp);
        Plateau plateau = new Plateau();
        plateau.setIdProjet(idProjet);
        List<Plateau> lp = dao.findWhere(plateau);
        model.addAttribute("lp", lp);
        return "listeplateau";
    }

    @RequestMapping(value = "/listescene", method = RequestMethod.GET)
    public String liste_scene(Model model, @RequestParam(required = true) Integer idPlateau) {
        Plateau plateau = dao.findById(Plateau.class, idPlateau);
        model.addAttribute("pl", plateau);
        Projet projet = dao.findById(Projet.class, plateau.getIdPlateau());
        model.addAttribute("p", projet);
        Scene s = new Scene();
        s.setIdPlateau(idPlateau);
        List<Scene> lsc = dao.findWhere(s);
        model.addAttribute("lsc", lsc);
        return "listescene";
    }

    @RequestMapping(value = "/getPlanning", method = RequestMethod.GET)
    public String getPlanning(Model model) {
        List<Planning> lp = dao.findAll(Planning.class);
        model.addAttribute("lp", lp);
        return "getplanning";
    }

    @RequestMapping(value = "/listeaction", method = RequestMethod.GET)
    public String liste_action(Model model, @RequestParam(required = true) Integer idScene) {
        Scene scene = dao.findById(Scene.class, idScene);
        model.addAttribute("lsc", scene);
        Plateau plateau = dao.findById(Plateau.class, scene.getIdPlateau());
        model.addAttribute("pl", plateau);
        Projet projet = dao.findById(Projet.class, plateau.getIdProjet());
        model.addAttribute("p", projet);
        Action a = new Action();
        a.setIdScene(idScene);
        List<Action> la = dao.findWhere(a);
        model.addAttribute("la", la);
        List<Emotion> emotion = dao.findAll(Emotion.class);
        model.addAttribute("le", emotion);
        List<Geste> geste = dao.findAll(Geste.class);
        model.addAttribute("lg", geste);
        List<Acteur> acteur = dao.findAll(Acteur.class);
        model.addAttribute("lact", acteur);
        return "listeaction";
    }

    @RequestMapping(value = "/updateActions")
    public String updateActions(Model model, HttpServletRequest request) {
        int idAction = Integer.parseInt(request.getParameter("idAction"));
        int idActeur = Integer.parseInt(request.getParameter("acteur"));
        int idGeste = Integer.parseInt(request.getParameter("geste"));
        int idScene = Integer.parseInt(request.getParameter("idScene"));
        int idEmotions = Integer.parseInt(request.getParameter("emotion"));
        String scenario = request.getParameter("scenario");
        Action action = new Action();
        action.setIdScene(idScene);
        action.setIdGeste(idGeste);
        action.setIdActeur(idActeur);
        action.setScenario(scenario);
        action.setIdEmotion(idEmotions);
        action.setIdAction(idAction);
        dao.update(action);
        model.addAttribute("id", idScene);
        return "redirect1";
    }

    @RequestMapping(value = "/ajoutAction")
    public String insertAction(Model model, HttpServletRequest request) {
        int idActeur = Integer.parseInt(request.getParameter("acteur"));
        int idGeste = Integer.parseInt(request.getParameter("geste"));
        int idScene = Integer.parseInt(request.getParameter("idScene"));
        int idEmotions = Integer.parseInt(request.getParameter("emotion"));
        String scenario = request.getParameter("scenario");
        Action action = new Action();
        action.setIdScene(idScene);
        action.setIdGeste(idGeste);
        action.setIdActeur(idActeur);
        action.setScenario(scenario);
        action.setIdEmotion(idEmotions);
        dao.create(action);
        model.addAttribute("id", idScene);
        return "redirect1";
    }

    @RequestMapping(value = "/insertionscene")
    public String insertScene(Model model, HttpServletRequest request) {
        int idScene = Integer.parseInt(request.getParameter("idPlateau"));
        String nom = request.getParameter("scenario");
        int duree = Integer.parseInt(request.getParameter("duree"));
        Scene scene = new Scene();
        scene.setNom(nom);
        scene.setDuree(duree);
        scene.setIdPlateau(idScene);
        dao.create(scene);
        model.addAttribute("id", idScene);
        return "redirect";
    }

    @RequestMapping(value = "/deleteScene")
    public String deleteScene(Model model, HttpServletRequest request) {
        int idplateau = Integer.parseInt(request.getParameter("idPlateau"));
        int idScene = Integer.parseInt(request.getParameter("idScene"));
        Scene scene = new Scene();
        scene.setIdScene(idScene);
        dao.delete(scene);
        model.addAttribute("id", idplateau);
        return "redirect";
    }

    @RequestMapping(value = "/deleteAction")
    public String deleteAction(Model model, HttpServletRequest request) {
        int idAction = Integer.parseInt(request.getParameter("idAction"));
        int idScene = Integer.parseInt(request.getParameter("idScene"));
        Action scene = new Action();
        scene.setIdAction(idAction);
        dao.delete(scene);
        model.addAttribute("id", idScene);
        return "redirect1";
    }
    
        @RequestMapping(value = "/plannings", method = RequestMethod.GET)
    public String planning(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        V_planning p = new V_planning();
        p.setIdProjet(idProjet);
        List<V_planning> lp = dao.findWhere(p);
        model.addAttribute("lp", lp);
        List<Scene> lsc = dao.findAll(Scene.class, "duree", true);
        model.addAttribute("lsc", lsc);
        return "getplanning";
    }

}
