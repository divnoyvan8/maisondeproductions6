<%-- 
    Document   : listeClient
    Created on : 3 f�vr. 2023, 16:46:49
    Author     : P14A_77_Michael
--%>

<%@page import="model.Geste"%>
<%@page import="model.Acteur"%>
<%@page import="model.Emotion"%>
<%@page import="dao.HibernateDao"%>
<%@page import="java.util.List"%>
<%@page import="model.Action"%>
<%@page import="model.Projet"%>
<%@page import="model.Plateau"%>
<%@page import="model.Scene"%>
<%@page import="utilitaires.*"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>


<%
    Projet p = (Projet) request.getAttribute("p");
    Plateau lp = (Plateau) request.getAttribute("pl");
    Scene lsc = (Scene) request.getAttribute("lsc");
    List<Action> la = (List<Action>) request.getAttribute("la");
    List<Emotion> le = (List<Emotion>) request.getAttribute("le");
    List<Acteur> lact = (List<Acteur>) request.getAttribute("lact");
    List<Geste> lg = (List<Geste>) request.getAttribute("lg");
%>
<html>

    <head>
        <meta charset="utf-8">
        <title>Article</title>
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/css1/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/responsive.css">
        <link rel="stylesheet" type="text/css"href="http://localhost:8080/Actu_-_Copie/assets/csss/Search.css">
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/Actu_-_Copie/assets/css/bootstrap.css" />
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Basic.css'); ?>">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Clean.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Registration-Form-with-Photo.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/ionicons.min.css">



        <%-- <% int pa;
             int s = 1 + (Integer) request.getAttribute("pages");
             if (request.getAttribute("page") == null) {
                 pa = 1;
             } else {
                 pa = (Integer) request.getAttribute("page");
             }
         %>  
        --%>
    </head>



    <body class="main-layout ">
        <header>
            <div class="header">

                <div class="container">
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo">
                                        <img src="http://localhost:8080/Actu_-_Copie/assets/images/logo1.png" alt="icon" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                            <div class="menu-area">
                                <div class="limit-box">
                                    <nav class="main-menu">
                                        <ul class="menu-area-main">
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a>  </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/">Acceuil</a>  </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/planning">Planning</a> </li>
                                            <li class="search">
                                                <form action="<%=request.getContextPath()%>/recherche">
                                                    <input style="background-color: black;border-radius: 20px;color:white; border: 0.5px" type="text" name="recherche">
                                                    <button style="background: black;border: 0px" type="submit" ><img type="submit" src="http://localhost:8080/Actu_-_Copie/assets/images/search_icon.png" alt="icon" /></button>
                                                </form>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-6">
                            <div class="location_icon_bottum">
                                <ul>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/call.png" />(+261)9876543109</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/email.png" />mymovies@gmail.com</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/loc.png" />mymovies</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>


    <body>

        <section class="bg-light" id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;">SCENE : <% out.println(lsc.getNom()); %></h4>
                        <h5 style="font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;">PLATEAU : <% out.println(lp.getNom()); %></h5><br>
                        <p class="text-muted">myaction</p>
                    </div>
                </div>

                <% for (int i = 0; i < la.size(); i++) {%>
                <div class="row">

                    <div class="col-sm-12 col-md-12 portfolio-item">

                        <div class="portfolio-caption">
                            <%--<h4>ACTION <% out.println(la.get(i).get); %></h4> --%>
   
                            <p class="text-muted"><strong></strong> <% out.println(Utilitaires.getNomActeur(la.get(i).getIdActeur()) + " avec " + Utilitaires.getNomEmotions(le.get(i).getIdEmotion())); %></p>
                            <p class="text-muted"><strong></strong><%out.println(" en " + Utilitaires.getNomGeste(lg.get(i).getIdGeste())); %></p>
                            <p class="text-muted"><strong></strong></p>
                            <p class="text-muted"><strong></strong><% out.println(la.get(i).getScenario());%></p>
                            <a style="font-size:19px;" href="<%=request.getContextPath()%>/deleteAction?idScene=<% out.println(lsc.getIdScene());%>&idAction=<% out.println(la.get(i).getIdAction());%>"><center> EFFACER CET ACTION
                                </center></a>
                            <a style="font-size:19px;" href="<%=request.getContextPath()%>/redirectionUpdate?scenario=<% out.println(la.get(i).getScenario());%>&idScene=<% out.println(lsc.getIdScene());%>&idAction=<% out.println(la.get(i).getIdAction());%>"><center> MODIFIER
                                </center></a>

                        </div>

                    </div>
                </div>
                <% }%>

                <form action="<%=request.getContextPath()%>/ajoutAction">
                    <div class="card-body">                                                     
                        <div class="row">
                            <div class="col">                                                                                     
                                <div class="mb-3"><input value="<%out.println(lsc.getIdScene());%>" name="idScene" hidden></div>                     
                                <label class="form-label"><strong>Scenario: </strong><br></label><textarea class="form-control" id="signature" rows="4" name="scenario"></textarea></p>
                                <div class="mb-3"><label class="form-label" for="type" style="color: #0b2e130;"><strong>Emotions : </strong></label></br>
                                    <select class="form-control-user" name="emotion">
                                        <%for (int i = 0; i < le.size(); i++) {
                                        %>
                                        <option value="<%out.print(le.get(i).getIdEmotion());%>"><%out.print(le.get(i).getNom());%></option>
                                        <%}%>
                                    </select>  </div> 
                                <label class="form-label" for="type" style="color: #0b2e130;"><strong>Geste : </strong></label></br>
                                <select class="form-control-user" name="geste">
                                    <%for (int i = 0; i < lg.size(); i++) {
                                    %>
                                    <option value="<%out.print(lg.get(i).getIdGeste());%>"><%out.print(lg.get(i).getNom());%></option>
                                    <%}%>
                                </select>                              
                                <div class="mb-3"><label class="form-label" for="type" style="color: #0b2e130;"><strong>Acteur : </strong></label></br>
                                    <select class="form-control-user" name="acteur">
                                        <%for (int i = 0; i < lact.size(); i++) {
                                        %>
                                        <option value="<%out.print(lact.get(i).getIdActeur());%>"><%out.print(lact.get(i).getNom());%></option>
                                        <%}%>
                                    </select>  </div> 
                                <a style="font-size:19px;" href="<%=request.getContextPath()%>/ajoutAction?idScene= <% out.println(lsc.getIdScene());%>"><center> <button  style="border-color: black;background-color: black"class="btn btn-primary d-block btn-user w-10" >AJOUTER ACTION
                                        </button></center></a>
                            </div>
                        </div> 
                    </div>           
                </form>

                <a style="font-size:19px;" href="<%=request.getContextPath()%>/deleteScene?idScene=<% out.println(lsc.getIdScene()); %>&idPlateau=<% out.println(lp.getIdPlateau());%>"><center>DELETE
                    </center></a><br>

                <a style="font-size:19px;" href="<%=request.getContextPath()%>/updateScene?idScene= <% out.println(lsc.getIdScene()); %>"><center>UPDATE
                    </center></a>





            </div>
        </section>










        <%
            // String date = sujet.get(i).getDatedepot();
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
            //LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
            /* String date2 = sujet.get(i).getDatepublication(); 
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                              LocalDateTime localDateTime1 = LocalDateTime.parse(date2, formatter);
                if ( localDateTime1.getHour() == LocalDateTime.now().getHour()
                        && localDateTime1.getMinute() == LocalDateTime.now().getMinute()) { 
                   out.println("mety");  */
        %>   



    </div>
</div>
</div>
</body>

<footer>
    <div id="contact" class="footer">
        <div class="container">
            <div class="row pdn-top-30">
                <div class="col-md-12 ">
                    <div class="footer-box">
                        <div class="headinga">
                            <h3>Address</h3>
                            <span>Paris | France </span>
                            <p>(+261) 8522369417
                                <br>jolympique@gmail.com</p>
                        </div>
                        <ul class="location_icon">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>

                        </ul>
                        <div class="menu-bottom">
                            <ul class="link">
                                <li> <a href="">Actualiser</a></li>
                                <li> <a href="<%=request.getContextPath()%>/login"> Deconnectez</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <marquee> <p>� RAKOTONANAHARY Maheritiana<a href="https://html.Michael/"> Michael</a> </p> </marquee>
                <marquee> <p>� VELOMORA Divno<a href="https://html.Michael/"> Yvan</a> </p> </marquee>
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/plugin.js"></script>

<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>

<script src="js/owl.carousel.js"></script>
</body>

</html>
