

<%@page import="java.util.ArrayList"%>
<%@page import="model.Projet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% ArrayList<Projet> lp = (ArrayList<Projet>) request.getAttribute("liste");%>
<html>

    <head>
        <meta charset="utf-8">
        <title>Article</title>
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/responsive.css">
         <link rel="stylesheet" type="text/css"href="http://localhost:8080/Actu_-_Copie/assets/csss/Search.css">
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/Actu_-_Copie/assets/css/bootstrap.css" />

                <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/bootstrap/css/bootstrap.min.css">
                 <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/ionicons.min.css">

    </head>

    <body class="main-layout ">
        <header>
            <div class="header">

                <div class="container">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                            <div class="menu-area">
                                <div class="limit-box">
                                    <nav class="main-menu">
                                        <ul class="menu-area-main">
                                            <li class="active"> <a style="font-size:19px;" href="<%=request.getContextPath()%>/">Accueil</a> </li>

                                        </ul>

                                    </nav>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-6">
                            <div class="location_icon_bottum">
                                <ul>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/call.png" />(+261)9876543109</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/email.png" />jolympique@gmail.com</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/loc.png" />Location</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        <section class="bg-light" id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;"> Resultats de recherche</h4><br>
                        <p class="text-muted">eto Madagasikara</p>
                    </div>
                </div>

                <div class="row">
                    <% for(int i=0;i<lp.size();i++){%>
                    <div class="col-sm-4 col-md-4 portfolio-item"><a class="portfolio-link" href="<%=request.getContextPath()%>/listeplateau?idProjet=<% out.println(lp.get(i).getIdProjet()); %>" data-bs-toggle="modal">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></i></div>
                            </div><img style="border-radius:6px;" class="img-fluid"  src="http://localhost:8080/Actu_-_Copie/assets/images/<% out.println(lp.get(i).getNomImage()); %>">
                        </a>
                        <div class="portfolio-caption">
                            <h4><% out.println(lp.get(i).getNom()); %></h4>
                            <p class="text-muted"><strong> </strong> <% out.println(lp.get(i).getSynopsis()); %> </p>

                        </div>
                    </div>
                    <% } %> 
                </div>


            </div>
        </section>

    </table>


    <footer>
        <div id="contact" class="footer">
            <div class="container">
                <div class="row pdn-top-30">
                    <div class="col-md-12 ">
                        <div class="footer-box">
                            <div class="headinga">
                                <h3>Address</h3>
                                <span>Paris | France </span>
                                <p>(+261) 8522369417
                                    <br>jolympique@gmail.com</p>
                            </div>

                            <div class="menu-bottom">
                                <ul class="link">
                                    <li> <a href="">Actualiser</a></li>
                                    <li> <a href="<%=request.getContextPath()%>/login"> Deconnectez</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <marquee> <p>Â© RAKOTONANAHARY Maheritiana<a href="https://html.Michael/"> Michael</a> 
                    <marquee> <p>© VELOMORA Divno<a href="https://html.Michael/"> Yvan</a> </p> </marquee>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/jquery-3.0.0.min.js"></script>
    <script src="js/plugin.js"></script>

    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/custom.js"></script>

    <script src="js/owl.carousel.js"></script>
</body>

</html>